#XActivity
#一个便于安卓开发的工具类库
####void	Call(java.lang.String number) 拨打=电话的方法

####void	download(java.lang.String url, NetResult result) 用于下载文件的函数
####java.lang.String	formatMemorySize(long memorySize) 将数字形式的内存格式化成易于理解的格式：1M，2G，3K

####void	Get(java.lang.String url, NetResult result) 通过Get的方式向服务器发送请求 并且返回数据
####java.util.List<SMS>	getAllSMS()  获取所有短信
####java.util.List<Contact>	getContacts()  获取所有联系人
####java.util.List<SMS>	getHasReadSMS()  获取已读短信
####java.util.List<SMS>	getHaveNotReadSMS() 获取未读短信
####java.io.File	getPhoneDataDirectory() 获取手机内部存储的File对象
####long	getPhoneFreeSize()   获取手机可用内存空间 
####long	getPhoneSize() 获取手机总内存空间 
####java.util.List<SMS>	getReceivedSms()  获取已接收短信
####java.io.File	getSDCardDirectory() 获取SD卡的File对象
####long	getSDcardFreeSize()  获取SD卡的可用大小 
####long	getSDCardSize()   获取SD卡的总大小 
####java.util.List<SMS>	getSentSms() 获取已发送短信
####SharedPreferences	GetSharedPreferences() 获取默认的SharedPreferences对象
####SharedPreferences	GetSharedPreferences(java.lang.String SharedPreferenceName) 获取自己设置的名称的SharedPreferences对象
####java.util.List<SMS>	getSMSByAddress(java.lang.String search_address)  获取指定号码的短信
####void	insertContacts(Contact... contacts)    向系统插入联系人
####void	insertSMS(SMS... smss)   向系统插入短信
####boolean	isNetWorkUseful() 检测网络状态是否可用
####boolean	isSDCardUseful() SD卡是否有用
####void	Post(java.lang.String actionUrl, java.util.Map<java.lang.String,java.lang.String> params, java.util.Map<java.lang.String,java.io.File> files, NetResult netResult)  通过Post方式上传文件以及提交表单
####void	Post(java.lang.String url, java.util.Map<java.lang.String,java.lang.String> params, NetResult result)  通过Post方式往服务器提交数据并获得返回数据
####void	PutSharedPreferences(SharedPreferences sharedPreferences, java.lang.String key, java.lang.Object value)  往指定的sharedPreferences里面存储数据
####void	PutSharedPreferences(java.lang.String key, java.lang.Object value) 往sharedPreferences里面存储数据
####boolean	sendSMS(java.lang.String number, java.lang.String content)    发送短信的方法
####void	SetSharedPreference(SharedPreferences sharedPreferences) 设置自己的sharedPreferences
####void	Show(int layout_ID) 显示自定义提示信息
####void	Show(java.lang.String string) 显示提示信息
####void	Show(View view)   显示自定义提示信息
####String getSharedPreferenceValue(String key) 从SharedPreference里面获取值 如果没有  则返回null空对象
####boolean isServiceRunning(String className) 检查指定服务是否正在运行
####boolean isServiceRunning(Class<?> className) 检查指定服务是否正在运行
#新增了一个入口com.xcode.core.AActivity
###继承此类从此再也可以不用写findViewById(R.id.xxx) setContentView(R.layout.xxx)
######此类继承自XActivity 拥有XActivity所有的方法 工具
####方便点在于通过注解进行初始化，当然你也可以依旧选择继承XActivity，<br>

		@setContentView(R.layout.activity_main)
		public class MainActivity extends AActivity
		{
			@findViewById(R.id.bu)
			Button button;

			@Override
			public void Init()
			{
				System.out.println("init");
				System.out.println(button);
			}
			@Override
			public void OnStart()
			{
				System.out.println("start");
			}
		}
####新增获取手机屏幕宽高 int getWindowHeigth()  int getWindowWidth()
####新增显示通知栏的方法 public void showNotifiCation(int IconID,String showmsg,String title,String content,Intent intent) com.xcode.util.Notification.showNotification
#### 新增常驻通知栏 不能被清除通知栏 点击即消失通知栏 showNotificationNoClear showNotificationAlwaysIn
####发送Get请求
		Get("http://www.baidu.com", new NetResult()
		{
			/**
			 * String content get请求获取到的内容
			 */
			@Override
			public void success(String content)
			{
				System.out.println("GET请求返回的数据");
			}
			@Override
			public void failed(int code)
			{
				System.out.println("请求失败 请求码为:"+code);
			}
		});
####发送POST请求
		//用一个map保存所有请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("name", "zhangsan");
		params.put("sex", "man");
		Post("http://www.baidu.com", params, new NetResult()
		{
			@Override
			public void success(String content)
			{
				System.out.println("POST请求返回的数据");
			}
			@Override
			public void failed(int code)
			{
				System.out.println("请求失败 请求码为:"+code);
			}
		});
####上传文件到服务器

		Map<String, String> params = new HashMap<String, String>();
		params.put("普通参数1", "123");
		params.put("普通参数2", "456");
		params.put("普通参数3", "小明");
		Map<String, File> files = new HashMap<String, File>();
		files.put("文件参数名1", new File("/sdcard/123.txt"));
		files.put("文件参数名2", new File("/sdcard/456.txt"));
		files.put("文件参数名3", new File("/sdcard/789.txt"));
		Post("http://192.168.1.200:8080/upload", params, files, new NetResult()
		{
			@Override
			public void success(String content)
			{
				System.out.println("文件上传成功！");
				System.out.println("服务器返回数据："+content);
			}
			@Override
			public void failed(int code)
			{
				System.out.println("文件上传失败 错误码:"+code);
			}
		});
	
####下载文件
		download("http://192.168.1.100:8080/123.exe", new NetResult()
		{
			@Override
			public void success(InputStream inputStream)
			{
				//保存下载下来的文件
				/**
				 * 	但是 此方法因为使用了网络inputstream，
				 *	所以是子线程 所以不能再此方法中直接更新界面UI 
				 */
			}
		});
####对SQLite数据库进行操作
		//新建一个表类 
		Table table1 = new Table("表名1");//设置表名
		table1.setKey("主键名", Table.paramType.INTEGER_NotNull);//设置主键名
		table1.addParam("name", Table.paramType.TEXT);	//设置字段 	以及类型
		table1.addParam("sex", Table.paramType.TEXT);	//设置字段 	以及类型
		//新建一个表类
		Table table2 = new Table("表名2");//设置表名
		table2.setKey("主键名", Table.paramType.INTEGER_NotNull);//设置主键名
		table2.addParam("name", Table.paramType.TEXT);//设置字段 	以及类型
		table2.addParam("sex", Table.paramType.TEXT);//设置字段 	以及类型
		//将两个表添加至DBUtil中 并设置数据库的名称
		DBUtil dbUtil = new DBUtil(this, "数据库名", table1,table2);
		
		ContentValues values = new ContentValues();
		values.put("name", "张三");
		values.put("sex", "男");
		//向表中添加一条字段
		dbUtil.insertInto("表名1", values);
####杀死应用程序 杀死自己应用程序 public void KillApplicationSelfe() 杀死其他应用程序 public void KillOtherApplication(String packageName)
####安装应用程序的方法 public void installAPk(String path) public void installAPk(File file)
####自动获取应用程序版本号的方法 public String getVersionName()
####新增一块公共区域 applicationContext 用于存取应用程序共享数据
	/**
	 * 向一块公共区域添加一个共享参数
	 * @param name		参数名
	 * @param object	参数
	 */
	public void setContextAttribute(String name,Object object)
	{
		ApplicationContext.setAttribute(name, object);
	}
	/**
	 * 向公共区域获取参数
	 * @param name	参数名
	 * @return		获取到的数据
	 */
	public Object getContextAttribute(String name)
	{
		return ApplicationContext.getAttribute(name);
	}
	/**
	 * 清空公共区域的所有数据
	 */
	public void ContextClear()
	{
		ApplicationContext.clear();
	}
	/**
	 * 向公共区域清除指定参数
	 * @param name	参数名
	 */
	public void ContextRemove(String name)
	{
		ApplicationContext.remove(name);
	}
####更新了数据库的操作 建立数据库.添加表
	
	import com.xcode.SQLite.annotation.Databases;
	import com.xcode.SQLite.annotation.Table;
	
	@Databases("数据库名")
	public class database
	{
		@Table("表名")
		Tab1 tb1;
	}
####实例化数据库
		
		DBFactory factory = DBFactory.initFactory(Context,database.class);

####向数据库中插入一条数据
		//这个是表的JavaBean
		Tab1 tab1 = new Tab1();
		factory.insert(tab1);
####查询一个表中所有数据
				
		List<Object> list2 = factory.query(Tab1.class);
		for (Object object : list2)
		{
			Tab1 tab = (Tab1) object;
			System.out.println(tab.getDouble());
		}
####条件查询一个表中的数据
		List<Object> list = factory.query("Int","string").from(Tab1.class).where("Int=1").limit(0, 10).commit();
		for (Object object : list)
		{
			Tab1 tab1 = (Tab1) object;
			System.out.println(tab1.getInt());
			System.out.println(tab1.getString());
		}
……正在更新

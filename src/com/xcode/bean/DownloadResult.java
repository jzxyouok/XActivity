package com.xcode.bean;


import java.io.InputStream;
/**
 * 然后提供访问网络两个成功和失败要调用的方法
 * @author 肖蕾
 */
public interface DownloadResult
{

	/**
	 * 回调函数  访问网络失败 返回码!=200 自动调用的函数
	 * @param code			错误码
	 */
	public void failed(int code);
	/**
	 * 回调函数  当访问网络请求成功 返回码=200 后自动调用的函数<br>
	 * 此处是下载文件的，<br>
	 * 所以直接在这里把文件的inputstream传进去进行调用<br>
	 * 输入流可以不要关闭。<br>
	 * 但是 此方法因为使用了网络inputstream，<br>
	 * 所以是子线程  所以不能再此方法中直接更新界面UI <br>
	 * @param inputStream			获取到的文件输入流
	 */
	public void success(InputStream inputStream,long fileLength);
}
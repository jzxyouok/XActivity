package com.xcode.bean;



/**
 * 对短信必要的属性进行封装 以便于对短信进行操作
 * 
 * @author 肖蕾
 */
public class SMS
{
	private String address;
	private long date;
	private int type;
	private String body;
	private int hasRead;
	public final static int SENT_SMS = 2;
	public final static int RECEVIED_SMS = 1;
	public final static int Has_Read = 1;
	public final static int Have_NOT_Read = 0;

	/**
	 * 初始化短信对象
	 * 
	 * @param address 	地址
	 * @param date 		发送/接收 日期
	 * @param type 		类型：已发送 SMS.SENT_SMS or 已接收 SMS.RECEVIED_SMS
	 * @param body 		短信内容
	 * @param hasread 	已读：已读 SMS.Has_Read or 未读 SMS.Have_NOT_Read
	 */
	public SMS(String address, long date, int type, String body, int hasread)
	{
		this.address = address;
		this.date = date;
		this.type = type;
		this.body = body;
		this.hasRead = hasread;
	}

	public void setHasRead(int hasRead)
	{
		this.hasRead = hasRead;
	}

	public int getHasRead()
	{
		return hasRead;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public String getBody()
	{
		return body;
	}

	public void setBody(String body)
	{
		this.body = body;
	}
}

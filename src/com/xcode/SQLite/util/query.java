package com.xcode.SQLite.util;

import java.lang.reflect.Field;
import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.xcode.SQLite.DBFactory;
import com.xcode.SQLite.annotation.Table;
import com.xcode.util.DBUtil;

public class query
{
	private String where = "";
	private String limit = "";
	private String tabname = "";
	private String Clomns = "";
	private String sqlSelect = "";
	private DBFactory factory = null;
	private SQLiteDatabase sqLiteDatabase = null;
	private Class<?> tabClass = null;
	
	
	public query setDBFactory(DBFactory factory)
	{
		this.factory = factory;
		if (factory == null)
		{
			throw new RuntimeException("没有实例化DBFactory");
		}else 
		{
			sqLiteDatabase = factory.getDbUtil().getSqliteDatabase();
		}
		return this;
	}
	/**
	 * 
	 * @param clomns 需要查询的字段
	 */
	public query(String ...clomns)
	{
		this.Clomns = "";
		if (clomns == null || (clomns != null && clomns.length == 1 && clomns[0].equals("")))
		{
			Clomns = "*";
		}else 
		{
			for (String clomn : clomns)
			{
				if (Clomns.isEmpty())
				{
					Clomns = clomn;
				}else 
				{
					Clomns+=(","+clomn);
				}
			}
		}
	}
	public query(){}
	
	public query where(String where)
	{
		this.where = "WHERE "+where;
		return this;
	}
	public query limit(int start,int count)
	{
		limit = "LIMIT "+start+","+count;
		return this;
	}
	public query from(String tabName)
	{
		this.tabname = tabName;
		return this;
	}
	public query from(Class<?> tabClass)
	{
		if (this.factory == null)
		{
			throw new RuntimeException("缺少DBFactory");
		}
		this.tabClass = tabClass;
		Class<?> databaseClass = factory.getDatabaseClass();
		Class<?> class1 = tabClass;
		Field [] fields = databaseClass.getDeclaredFields();
		for (Field field : fields)
		{
			if (field.getType().equals(class1))
			{
				if (field.isAnnotationPresent(Table.class))
				{
					Table table = field.getAnnotation(Table.class);
					String tabNameString = "";
					if (table.value().isEmpty())
					{
						tabNameString = field.getType().getSimpleName();
					}else
					{
						tabNameString = table.value();
					}
					this.tabname = tabNameString;
					break;
				}
			}
		}
		
		return this;
	}
	public List<Object> commit()
	{
		sqlSelect = "SELECT "+Clomns+" FROM "+tabname+" "+where+" "+limit;
		System.out.println(sqlSelect);
		Cursor cursor = sqLiteDatabase.rawQuery(sqlSelect, null);
		
		return DBUtil.DB2JavaBean(cursor, tabClass);
	}
	public List<Object> commit(Class<?> class1)
	{
		tabClass = class1;
		return this.commit();
	}
	
}
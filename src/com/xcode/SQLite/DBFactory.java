package com.xcode.SQLite;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.xcode.SQLite.annotation.Databases;
import com.xcode.SQLite.annotation.Table;
import com.xcode.SQLite.annotation.TableID;
import com.xcode.util.DBUtil;

/**
 * 一个新的数据库工厂
 * 用于提供对数据库的操作的简洁方法
 * @author 肖蕾
 *
 */
public class DBFactory
{
	private DBFactory(){}
	private Class<?> databaseClass = null;
	private Context context = null;
	private DBUtil dbUtil = null;
	
	public Context getContext()
	{
		return context;
	}
	public DBUtil getDbUtil()
	{
		return dbUtil;
	}
	public Class<?> getDatabaseClass()
	{
		return databaseClass;
	}
	
	/**
	 * 初始化一个DBFactory
	 * @param context		上下文
	 * @param databaseClass	数据库类
	 * @return				一个DBFactory实例
	 */
	public static DBFactory initFactory(Context context,Class<?> databaseClass)
	{
		DBFactory dbFactory = new DBFactory();
		dbFactory.databaseClass = databaseClass;
		dbFactory.context = context;
		dbFactory.init();
		return dbFactory;
	}
	
	private void init()
	{
		Databases databases = databaseClass.getAnnotation(Databases.class);
		/**
		 * 获取里面所有的变量
		 */
		List<com.xcode.bean.Table> tables = new ArrayList<com.xcode.bean.Table>();
		Field [] fields = databaseClass.getDeclaredFields();
		for (Field field : fields)
		{
			if (field.isAnnotationPresent(Table.class))
			{
				String tabName = null;
				Table tableName = field.getAnnotation(Table.class);
				Class<?> class1 = field.getType();
				com.xcode.bean.Table table = null;
				if (tableName.value().isEmpty())
				{
					tabName = class1.getSimpleName();
				}else 
				{
					tabName = tableName.value();
				}
				table = new com.xcode.bean.Table(tabName);
				Field[] fields2 = class1.getDeclaredFields();
				for (Field field2 : fields2)
				{
					if (field2.isAnnotationPresent(TableID.class))
					{
						table.setKey(field2.getName(), com.xcode.bean.Table.paramType.INTEGER_NotNull);
					}else 
					{
						table.addParam(field2.getName(), com.xcode.bean.Table.paramType.TEXT);
					}
				}
				tables.add(table);
			}
		}
		if (!databases.value().isEmpty())
		{
			dbUtil = new DBUtil(context, databases.value(),tables.toArray(new com.xcode.bean.Table[tables.size()]));
		}else 
		{
			dbUtil = new DBUtil(context, databaseClass.getSimpleName(),tables.toArray(new com.xcode.bean.Table[tables.size()]));
		}
	}
	/**
	 * 向数据库中插入一条数据
	 * @param object	表的类的实例对象
	 */
	public void insert(Object object)
	{
		Class<?> class1 = object.getClass();
		Field [] fields = databaseClass.getDeclaredFields();
		for (Field field : fields)
		{
			if (field.getType().equals(class1))
			{
				if (field.isAnnotationPresent(Table.class))
				{
					Table table = field.getAnnotation(Table.class);
					String tabNameString = "";
					if (table.value().isEmpty())
					{
						tabNameString = field.getType().getSimpleName();
					}else
					{
						tabNameString = table.value();
					}
					ContentValues values = new ContentValues();
					Class<?> fieldClass = field.getType();
					Field [] fields2 = fieldClass.getDeclaredFields();
					Method [] methods = fieldClass.getDeclaredMethods();
					for (Field field2 : fields2)
					{
						if (field2.isAnnotationPresent(TableID.class))
						{
							continue;
						}
						for (Method method : methods)
						{
							if (!method.getReturnType().toString().toLowerCase().equals("void"))
							{
								
								if (method.getName().toLowerCase().endsWith(field2.getName().toLowerCase()) && 
										(method.getName().toLowerCase().startsWith("get") || method.getName().toLowerCase().startsWith("is") )
										)
								{
									try
									{
										Object return_obj = method.invoke(object, null);
										String value = "";
										if (return_obj != null)
										{
											value = return_obj.toString();
										}
										values.put(field2.getName(),value);
										
									} catch (Exception e)
									{
										e.printStackTrace();
									} 
								}
							}
						}
					}
					
					dbUtil.insertInto(tabNameString, values);
				}
			}
		}
		
	}
	
	/**
	 * 查询指定表中的全部数据并以对象集合的方式返回
	 * @param queryTabClass		需要查询的表
	 * @return					对象的list集合
	 */
	public List<Object> query(Class<?> queryTabClass)
	{
		Field [] tableFields = databaseClass.getDeclaredFields();
		Method [] queryMethods = queryTabClass.getDeclaredMethods();
		List<Object> list = null;
		for (Field tabField : tableFields)
		{
			if (tabField.getType().equals(queryTabClass))//遍历数据库  找到需要查询的对应的表
			{
				Table table = tabField.getAnnotation(Table.class);
				String tableName = (table.value().isEmpty()?queryTabClass.getSimpleName():table.value());
				System.out.println("表名："+tableName);
				list = new ArrayList<Object>();
				SQLiteDatabase sqLiteDatabase = dbUtil.getSqliteDatabase();
				Cursor cursor = dbUtil.selectAllByTableName(tableName, sqLiteDatabase);
				while (cursor.moveToNext())
				{
					String columnNames[] = cursor.getColumnNames();
					Object object = null;
					try
					{
						object = queryTabClass.newInstance();
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					for (String columnName : columnNames)
					{
						String value = cursor.getString(cursor.getColumnIndex(columnName));
						for (Method queryMethod : queryMethods)
						{
							if (queryMethod.getReturnType().toString().equals("void") &&
									queryMethod.getName().startsWith("set")&&
									queryMethod.getName().toLowerCase().endsWith(columnName.toLowerCase())
									)
							{
								Class<?> class1 = queryMethod.getParameterTypes()[0];
								
								String paramType = class1.getSimpleName();
								System.out.print(queryMethod.getName()+" ");
								System.out.print(columnName+" "+value);
								System.out.println(" "+class1.getSimpleName());
								try
								{
									if (paramType.equals(int.class.getSimpleName()))
									{
										queryMethod.invoke(object, new Object[]{Integer.parseInt(value)});
									}
									else if (paramType.equals(float.class.getSimpleName())) 
									{
										queryMethod.invoke(object, new Object[]{Float.parseFloat(value)});
									}
									else if (paramType.equals(char.class.getSimpleName())) 
									{
										char charvalue = value.toCharArray()[0];
										queryMethod.invoke(object, new Object[]{charvalue});
									}
									else if (paramType.equals(double.class.getSimpleName())) 
									{
										queryMethod.invoke(object, new Object[]{Double.parseDouble(value)});
									}
									else if (paramType.equals(byte.class.getSimpleName())) 
									{
										queryMethod.invoke(object, new Object[]{Byte.parseByte(value)});
									}
									else if (paramType.equals(long.class.getSimpleName())) 
									{
										queryMethod.invoke(object, new Object[]{Long.parseLong(value)});
									}
									else if (paramType.equals(boolean.class.getSimpleName())) 
									{
										queryMethod.invoke(object, new Object[]{Boolean.parseBoolean(value)});
									}
									else if (paramType.equals(String.class.getSimpleName())) 
									{
										queryMethod.invoke(object, new Object[]{value});
									}
								} catch (Exception e)
								{
									e.printStackTrace();
								} 
								
							}
						}
					}
					System.out.println("-------------------------");
					list.add(object);
				}
			}
		}
		return list;
	}
	
	/**
	 * 条件查询
	 * @param cloms
	 * @return	一个query对象，使用里面的方法添加查询条件 query.commit() 提交事物
	 */
	public com.xcode.SQLite.util.query query(String... cloms)
	{
		return new com.xcode.SQLite.util.query(cloms).setDBFactory(this);
	}
	/**
	 * 条件查询
	 * @return	一个query对象，使用里面的方法添加查询条件 query.commit() 提交事物
	 */
	public com.xcode.SQLite.util.query query()
	{
		return new com.xcode.SQLite.util.query().setDBFactory(this);
	}
	
	
	public void update(Object oldData,Object newData)
	{
		
	}
	public void delete(Object data)
	{
	}
	
}

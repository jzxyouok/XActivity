package com.xcode.util;

import com.xcode.util.interface_.ViewSize;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;


/**
 * @author 肖蕾
 */
public class ViewUtil
{
	/**
	 * 获取view控件的size 在OnCreate中
	 * @param view
	 * @param viewSize
	 */
	public static void getViewSize(final View view,final ViewSize viewSize)
	{
		ViewTreeObserver vto = view.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener()
		{
			@Override
			public void onGlobalLayout()
			{
				view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				viewSize.Size(view.getWidth(), view.getHeight());
			}
		});
		vto = null;
	}
}

package com.xcode.util;

import android.content.Context;
import android.telephony.TelephonyManager;

public class TelephonyManagerUtil
{
	public static class SIMCard
	{
		/**
		 * 获取手机卡的运营商
		 * @param context	上下文
		 * @return			中文形式的 “中国移动”“中国联通”"中国电信"
		 */
		public static String GetSimName(Context context)
		{
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String imsi = telephonyManager.getSubscriberId();
			if (imsi != null)
			{
				if (imsi.equals("46000") || imsi.equals("46002"))
				{
					//移动
					return "中国移动";
				}else if (imsi.equals("46001"))
				{
					//联通
					return "中国联通";
				} else if (imsi.equals("46003")) 
				{
					//电信
					return "中国电信";
				}
			}
			return null;
		}
	}
}

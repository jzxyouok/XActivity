package com.xcode.util;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext
{
	private static Map<String, Object> context = new HashMap<String, Object>();
	/**
	 * 向公共区域保存参数
	 * @param name		参数名
	 * @param object	保存的参数
	 */
	public static void setAttribute(String name,Object object)
	{
		context.put(name, object);
	}
	/**
	 * 向公共区域获取参数
	 * @param name	参数名
	 * @return		获取的对象
	 */
	public static Object getAttribute(String name)
	{
		return context.get(name);
	}
	/**
	 * 移除一个数据
	 * @param name	参数名
	 */
	public static void remove(String name)
	{
		context.remove(name);
	}
	/**
	 * 清空所有数据
	 */
	public static void clear()
	{
		context.clear();
	}
}

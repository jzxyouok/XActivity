package com.xcode.util.audio;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;


public class MyAudioTrack extends AudioTrack
{
	/**
	 * 
	 * @param sampleRateInHzthe  	44100, 22050 and 11025.
	 * @param channelConfig			推荐 AudioFormat.CHANNEL_OUT_MONO and AudioFormat.CHANNEL_OUT_STEREO
	 * @param audioFormat			AudioFormat.ENCODING_PCM_16BIT 音质高 AudioFormat.ENCODING_PCM_8BIT 音质低功耗低
	 * @param mode					AudioTrack.MODE_STATIC and AudioTrack.MODE_STREAM
	 */
	public MyAudioTrack(int sampleRateInHz, int channelConfig,int audioFormat, int mode)
	{
		super(AudioManager.STREAM_MUSIC, sampleRateInHz, channelConfig, audioFormat,AudioTrack.getMinBufferSize(sampleRateInHz,channelConfig, audioFormat), mode);
	}
	
	/**
	 * @return 获得一个推荐配置的播放声音的对象
	 */
	public static MyAudioTrack getDefaultAudioTrack()
	{
		return new MyAudioTrack(44100,AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,AudioTrack.MODE_STREAM);
	}
	/**
	 * 开始播放
	 */
	public void start()
	{
		this.play();// 开始播放
	}
	
	public void Play(byte [] data)
	{
		this.write(data, 0, data.length);
	}
	/**
	 * 释放资源
	 */
	public void destory()
	{
		this.release();
	}
}

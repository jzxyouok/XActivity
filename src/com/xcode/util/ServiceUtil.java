package com.xcode.util;

import java.util.List;
import android.app.ActivityManager;
import android.content.Context;
/**
 * 对服务的一个工具类
 * @author 肖蕾
 */
public class ServiceUtil
{
	/**
	 * 用来判断服务是否运行.<br>
	 * 首先获取系统所有的正在运行的service<br>
	 * 然后遍历 如果找到自己要判断的service<br>
	 * @param context		上下文
	 * @param className 	需要判断的服务名字：包名+类名
	 * @return true 在运行, false 不在运行
	 */

	public static boolean isServiceRunning(Context context, String className)
	{

		boolean isRunning = false;

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		
		List<ActivityManager.RunningServiceInfo> serviceList= activityManager.getRunningServices(Integer.MAX_VALUE);
		
		if (!(serviceList.size() > 0))
		{
			return false;
		}
		for (int i = 0; i < serviceList.size(); i++)
		{
			if (serviceList.get(i).service.getClassName().equals(className))
			{
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}
	
	/**
	 * 用来判断服务是否运行.<br>
	 * 首先获取系统所有的正在运行的service<br>
	 * 然后遍历 如果找到自己要判断的service<br>
	 * @param context		上下文
	 * @param className 	需要判断的服务名字：包名+类名
	 * @return true 在运行, false 不在运行
	 */
	public static boolean isServiceRunning(Context context, Class<?> className)
	{
		
		boolean isRunning = false;
		
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		
		List<ActivityManager.RunningServiceInfo> serviceList= activityManager.getRunningServices(Integer.MAX_VALUE);
		
		if (!(serviceList.size() > 0))
		{
			return false;
		}
		for (int i = 0; i < serviceList.size(); i++)
		{
			if (serviceList.get(i).service.getClassName().equals(className.getName()))
			{
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}
}

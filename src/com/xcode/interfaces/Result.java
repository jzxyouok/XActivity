package com.xcode.interfaces;

public interface Result
{
	/**
	 * 回调函数  当访问网络请求成功 返回码=200 后自动调用的函数
	 * @param content		请求网络成功后返回的数据
	 */
	public void success(String content);
	/**
	 * 回调函数  访问网络失败 返回码!=200 自动调用的函数
	 * @param code			错误码
	 */
	public void failed(int code);
}

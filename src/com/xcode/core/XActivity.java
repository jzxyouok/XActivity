package com.xcode.core;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.xcode.bean.Contact;
import com.xcode.bean.DownloadResult;
import com.xcode.bean.SMS;
import com.xcode.interfaces.Result;
import com.xcode.util.ActivityManagerUtil;
import com.xcode.util.AndroidUtil;
import com.xcode.util.ApplicationContext;
import com.xcode.util.ExitApplication;
import com.xcode.util.Notification;
import com.xcode.util.TelephonyManagerUtil;
import com.xcode.util.WindowUtil;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

/**
 * Activity需要继承的类
 * @author 肖蕾
 */
public class XActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		ExitApplication.getInstance().addActivity(this);
	}
	/**
	 * 向一块公共区域添加一个共享参数
	 * @param name		参数名
	 * @param object	参数
	 */
	public void setContextAttribute(String name,Object object)
	{
		ApplicationContext.setAttribute(name, object);
	}
	/**
	 * 向公共区域获取参数
	 * @param name	参数名
	 * @return		获取到的数据
	 */
	public Object getContextAttribute(String name)
	{
		return ApplicationContext.getAttribute(name);
	}
	/**
	 * 清空公共区域的所有数据
	 */
	public void ContextClear()
	{
		ApplicationContext.clear();
	}
	/**
	 * 向公共区域清除指定参数
	 * @param name	参数名
	 */
	public void ContextRemove(String name)
	{
		ApplicationContext.remove(name);
	}
	/**
	 * 获取应用程序的版本号
	 * @return			应用程序的版本号
	 */
	public String getVersionName()
	{
		return AndroidUtil.Application.getVersionName(this);
	}
	/**
	 * 安装apk文件
	 * @param path	apk文件的路径
	 */
	public void installAPk(String path)
	{
		AndroidUtil.Application.installAPk(this, path);
	}
	/**
	 * 安装apk文件
	 * @param file	apk文件的file对象
	 */
	public void installAPk(File file)
	{
		AndroidUtil.Application.installAPk(this, file);
	}
	/**
	 * 获取手机的运营商中文名称
	 * @return	中文形式的 “中国移动”“中国联通”"中国电信"
	 */
	public String GetSimName()
	{
		return TelephonyManagerUtil.SIMCard.GetSimName(this);
	}
	
	/**
	 * 将自己应用程序完全退出进程的方法
	 */
	public void KillApplicationSelfe()
	{
		ActivityManagerUtil.KillApplicationSelfe();
	}
	
	/**
	 * 将自己应用程序完全退出进程的方法<br>
	 * 但是必须在manifest中添加权限<br>
	 * <b>android.permission.KILL_BACKGROUND_PROCESSES</b>
	 * @param packageName	需要杀死的应用程序报名
	 */
	public void KillOtherApplication(String packageName)
	{
		ActivityManagerUtil.KillOtherApplication(this, packageName);
	}
	
	/**
	 * 结束整个应用程序
	 */
	public void ExitApplication()
	{
		ExitApplication.getInstance().exit();
	}
	/**
	 * 在系统栏上显示一个点击自动消失的提示信息
	 * @param NotuifiID 提示栏的ID
	 * @param IconID	图标ID
	 * @param showmsg	未打开显示滚动的文字
	 * @param title		拖下任务栏 看见的标题
	 * @param content	拖下任务栏 看见的文本
	 * @param intent	点击提示信息将要执行的操作
	 */
	public void showNotification(int NotuifiID,int IconID,String showmsg,String title,String content,Intent intent)
	{
		Notification.showNotification(this,NotuifiID, IconID, showmsg, title, content, intent);
	}
	/**
	 * 在系统栏上显示一个不会被清理的提示信息
	 * @param NotuifiID 提示栏的ID
	 * @param IconID	图标ID
	 * @param showmsg	未打开显示滚动的文字
	 * @param title		拖下任务栏 看见的标题
	 * @param content	拖下任务栏 看见的文本
	 * @param intent	点击提示信息将要执行的操作
	 */
	public void showNotificationNoClear(int NotuifiID,int IconID,String showmsg,String title,String content,Intent intent)
	{
		Notification.showNotificationNoClear(this,NotuifiID, IconID, showmsg, title, content, intent);
	}
	/**
	 * 在系统栏上显示一个一直存在的的提示信息
	 * @param NotuifiID 提示栏的ID
	 * @param IconID	图标ID
	 * @param showmsg	未打开显示滚动的文字
	 * @param title		拖下任务栏 看见的标题
	 * @param content	拖下任务栏 看见的文本
	 * @param intent	点击提示信息将要执行的操作
	 */
	public void showNotificationAlwaysIn(int NotuifiID,int IconID,String showmsg,String title,String content,Intent intent)
	{
		Notification.showNotificationAlwaysIn(this,NotuifiID, IconID, showmsg, title, content, intent);
	}
	/**
	 * 获取屏幕高度
	 * @return 屏幕高度
	 */
	public int getWindowHeigth()
	{
		return WindowUtil.getWindowHeight(this);
	}
	/**
	 * 获取屏幕宽度
	 * @return 屏幕宽度
	 */
	public int getWindowWidth()
	{
		return WindowUtil.getWindowWindth(this);
	}
	
	/**
	 * 发送短信的方法<br>
	 * 需要添加发送短信的权限<br>
	 * <b>android.permission.SEND_SMS</b>
	 * @param number	发送的号码
	 * @param content	发送的内容
	 * @return			是否发送成功
	 */
	public boolean sendSMS(String number,String content)
	{
		return com.xcode.util.SMSUtil.sendSMS(XActivity.this, number, content);
	}
	/**
	 * 发送短信的方法<b>【适用群发】</b><br>
	 * 需要添加发送短信的权限<br>
	 * <b>android.permission.SEND_SMS</b>
	 * @param numbers	需要发送的短信的多个号码
	 * @param content	发送的内容
	 * @return			是否发送成功
	 */
	public boolean sendSMS(String []numbers,String content)
	{
		return com.xcode.util.SMSUtil.sendSMS(XActivity.this, numbers, content);
	}
	/**
	 * 拨打=电话的方法<br>
	 * 需要添加拨号的权限<br>
	 * <b>android.permission.CALL_PHONE</b>
	 * @param number	拨打的手机号
	 */
	public void Call(String number)
	{
		 //用intent启动拨打电话  
        Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+number));  
        startActivity(intent);  
	}
	/**
	 * 显示提示信息
	 * 
	 * @param string 提示信息
	 */
	public void Show(String string)
	{
		com.xcode.util.Toast.Show(XActivity.this, string);
	}

	/**
	 * 显示自定义提示信息
	 * 
	 * @param layout_ID 自定义提示信息的id
	 */
	public void Show(int layout_ID)
	{
		com.xcode.util.Toast.Show(XActivity.this, layout_ID);
	}

	/**
	 * 显示自定义提示信息
	 * 
	 * @param view 自定义提示信息的view对象
	 */
	public void Show(View view)
	{
		com.xcode.util.Toast.Show(XActivity.this, view);
	}

	/**
	 * 从SharedPreference里面获取值
	 * @param key	存在SharedPreference里面值的key
	 * @return		获取到的value 如果没有  则返回null空对象
	 */
	public String getSharedPreferenceValue(String key)
	{
		return com.xcode.util.sharedPreferences.getSharedPreferenceValue(XActivity.this,key);
	}
	/**
	 * 从指定的sharedPreferences对象里面获取值
	 * @param sharedPreferences		指定的sharedPreferences对象
	 * @param key					对应的key
	 * @return						获取到的value 如果没有  则返回null空对象
	 */
	public String getSharedPreferenceValue(SharedPreferences sharedPreferences,String key)
	{
		return com.xcode.util.sharedPreferences.getSharedPreferenceValue(sharedPreferences,key);
	}
	
	/**
	 * 往sharedPreferences里面存储数据
	 * 
	 * @param key 		key
	 * @param value     值
	 * @return 			存入成功则返回true 否则返回false
	 */
	public boolean PutSharedPreferences(String key, Object value)
	{
		return com.xcode.util.sharedPreferences.PutSharedPreferences(XActivity.this, key, value);
	}

	/**
	 * 往指定的sharedPreferences里面存储数据
	 * 
	 * @param sharedPreferences 	指定的sharedPreferences对象
	 * @param key 					存储的key
	 * @param value 				存储的值
	 * @return 						存储成功返回true 否则返回false
	 */
	public boolean PutSharedPreferences(SharedPreferences sharedPreferences, String key, Object value)
	{
		return com.xcode.util.sharedPreferences.PutSharedPreferences(sharedPreferences, key, value);
	}

	/**
	 * 获取默认的SharedPreferences对象
	 * @return	返回SharedPreferences对象
	 */
	public SharedPreferences GetDefaultSharedPreferences()
	{
		return com.xcode.util.sharedPreferences.GetDefaultSharedPreferences(XActivity.this);
	}

	/**
	 * 获取自己设置的名称的SharedPreferences对象
	 * 
	 * @param SharedPreferenceName 	SharedPreference文件名字
	 * @return	返回自己设置的名称的SharedPreferences对象
	 */
	public SharedPreferences GetSharedPreferences(String SharedPreferenceName)
	{
		return com.xcode.util.sharedPreferences.GetSharedPreferences(XActivity.this, SharedPreferenceName);
	}

	/**
	 * SD卡是否有用
	 * 
	 * @return true 有用 false 没有
	 */
	public boolean isSDCardUseful()
	{
		return com.xcode.util.phoneMemory.isSDCardUseful();
	}

	/**
	 * 获取SD卡的File对象
	 * 
	 * @return SD卡的File对象
	 */
	public File getSDCardDirectory()
	{
		return com.xcode.util.phoneMemory.getSDCardDirectory();
	}

	/**
	 * 获取手机内部存储的File对象
	 * 
	 * @return 手机内部存储的File对象
	 */
	public File getPhoneDataDirectory()
	{
		return com.xcode.util.phoneMemory.getPhoneDataDirectory();
	}

	/**
	 * 获取SD卡的总大小 为什么不直接返回格式化的<br>
	 * 因为可能开发中需要内存的做比较
	 * @return SD卡的总大小
	 */
	public long getSDCardSize()
	{
		return com.xcode.util.phoneMemory.getSDCardSize();
	}

	/**
	 * 获取SD卡的可用大小 为什么不直接返回格式化<br> 
	 * 因为可能开发中需要内存的做比较
	 * @return SD卡的可用大小
	 */
	public long getSDcardFreeSize()
	{
		return com.xcode.util.phoneMemory.getSDcardFreeSize();
	}

	/**
	 * 获取手机总内存空间 为什么不直接返回格式化<br> 
	 * 因为可能开发中需要内存的做比较
	 * @return 手机总的内存空间
	 */
	public long getPhoneSize()
	{
		return com.xcode.util.phoneMemory.getPhoneSize();
	}

	/**
	 * 获取手机可用内存空间 为什么不直接返回格式化<br> 
	 * 因为可能开发中需要内存的做比较
	 * 
	 * @return 手机可用内存空间
	 */
	public long getPhoneFreeSize()
	{
		return com.xcode.util.phoneMemory.getPhoneFreeSize();
	}

	/**
	 * 将数字形式的内存格式化成易于理解的格式：1M，2G，3K
	 * 
	 * @param memorySize 	内存大小
	 * @return 				格式化之后的字符串
	 */
	public String formatMemorySize(long memorySize)
	{
		return com.xcode.util.phoneMemory.formatMemorySize(XActivity.this, memorySize);
	}

	/**
	 * 获取所有短信
	 * 
	 * @since 	对于短信的类型Type 接收到的是1 发出去的是2
	 * @return 所有短信的SMS的list集合
	 */
	public List<SMS> getAllSMS()// 获取所有短信
	{
		return com.xcode.util.SMSUtil.getAllSMS(XActivity.this);
	}

	/**
	 * 获取已发送短信
	 * 
	 * @since 对于短信的类型Type 接收到的是1 发出去的是2
	 * @return	返回短信对象list集合
	 */
	public List<SMS> getSentSms()// 获取已发送短信
	{
		return com.xcode.util.SMSUtil.getSentSms(XActivity.this);
	}

	/**
	 * 获取已接收短信
	 * @since 对于短信的类型Type 接收到的是1 发出去的是2
	 * @return	返回接收到的短信list集合
	 */
	public List<SMS> getReceivedSms()// 获取已接收短信
	{
		return com.xcode.util.SMSUtil.getReceivedSms(XActivity.this);
	}

	/**
	 * 获取已读短信
	 * @return 已读短信的SMS对象list集合
	 */
	public List<SMS> getHasReadSMS()
	{
		return com.xcode.util.SMSUtil.getHasReadSMS(XActivity.this);
	}

	/**
	 * 获取未读短信
	 * 
	 * @return 未读短信的SMS对象list集合
	 */
	public List<SMS> getHaveNotReadSMS()
	{
		return com.xcode.util.SMSUtil.getHaveNotReadSMS(XActivity.this);
	}

	/**
	 * 获取指定号码的短信
	 * 
	 * @param search_address 	需要查询的号码
	 * @return 					指定号码的SMS对象List集合
	 */
	public List<SMS> getSMSByAddress(String search_address)
	{
		return com.xcode.util.SMSUtil.getSMSByAddress(XActivity.this, search_address);
	}

	/**
	 * 向系统插入短信
	 * @param smss	短信对象，空  或者短信对象数组
	 * @return 		插入成功返回true 否则  false
	 */
	public boolean insertSMS(SMS... smss)
	{
		return com.xcode.util.SMSUtil.insertSMS(XActivity.this, smss);
	}

	/**
	 * 获取所有联系人
	 * @return 对联系人信息封装的Contact的类
	 */
	public List<Contact> getContacts()
	{
		return com.xcode.util.contactUtil.getContacts(XActivity.this);
	}

	/**
	 * 向系统插入联系人
	 * @param contacts 一个或多个联系人，或联系人数组
	 * @return 		插入联系人成功返回true 否则返回false
	 */
	public boolean insertContacts(Contact... contacts)
	{
		return com.xcode.util.contactUtil.insertContacts(XActivity.this, contacts);
	}
	
	/**
	 * 通过Get的方式向服务器发送请求 并且返回数据
	 * @param url 		请求的链接
	 * @param result 	请求的结果对象
	 */
	public void Get(final String url, final Result result)
	{
		com.xcode.util.NetworkUtil.Get(XActivity.this, url, result);
	}
	
	/**
	 * 用于下载文件的函数
	 * @param url		下载链接
	 * @param result	请求网络的结果以便做不同的处理
	 */
	public void download(final String url,final DownloadResult result)
	{
		com.xcode.util.NetworkUtil.download(XActivity.this, url, result);
	}
	
	/**
	 * 通过Post方式往服务器提交数据并获得返回数据
	 * @param url 		访问的URL
	 * @param params 	需要传递的参数
	 * @param result 	代表请求结果的对象
	 */
	public void Post(final String url, final Map<String, String> params, final Result result)
	{
		com.xcode.util.NetworkUtil.Post(XActivity.this, url, params, result);
	}
	
	/**
	 * 通过Post方式上传文件以及提交表单
	 * @param actionUrl		为要使用的URL
	 * @param params		为表单内容	name_value
	 * @param files			参数为要上传的文件，可以上传多个文件 name_file
	 */
	public void Post(final String actionUrl, final Map<String, String> params,final Map<String, File> files,final Result netResult)
	{
		com.xcode.util.NetworkUtil.Post(XActivity.this, actionUrl, params, files, netResult);
	}
	/**
	 * 检测网络状态是否可用
	 * @return	如果可用则返回true 否则false
	 */
	public boolean isNetWorkUseful()
	{
		return com.xcode.util.NetworkUtil.isNetWorkUseful(XActivity.this);
	}
	/**
	 * 判断指定的服务是否在运行
	 * @param className		服务的限定名
	 * @return				如果正在运行 true 否则  false
	 */
	public boolean isServiceRunning(String className)
	{
		return com.xcode.util.ServiceUtil.isServiceRunning(XActivity.this, className);
	}
	/**
	 * 判断指定的服务是否在运行
	 * @param className		服务的限定名
	 * @return				如果正在运行 true 否则  false
	 */
	public boolean isServiceRunning(Class<?> className)
	{
		return com.xcode.util.ServiceUtil.isServiceRunning(XActivity.this, className);
	}
}
